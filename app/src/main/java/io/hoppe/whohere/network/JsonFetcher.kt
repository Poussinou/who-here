/*
Who Here
Copyright (C) 2020  Josef Hoppe

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published 
by the Free Software Foundation; either version 2 of the License, 
or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package io.hoppe.whohere.network

import android.os.AsyncTask
import com.fasterxml.jackson.databind.ObjectMapper
import java.net.URL

class JsonFetcher<T>(val handle: (result: T) -> Unit, val handleError: () -> Unit) : AsyncTask<Object, Void,  T>() {
    override fun doInBackground(vararg args: Object?): T? {
        try {
            //check arguments
            if(args.size == 2){
                val url = args[0] as Any?
                val javaClass = args[1] as Any?
                if(url != null && url is URL && javaClass != null && javaClass is Class<*>) {
                    //main implementation
                    val mapper = ObjectMapper()
                    return mapper.readValue(url, javaClass) as T
                }
            }
            return null
        } catch (e : Exception) {
            return null
        }

    }

    override fun onPostExecute(result: T) {
        if(result != null) {
            handle(result)
        } else {
            handleError()
        }
    }
}