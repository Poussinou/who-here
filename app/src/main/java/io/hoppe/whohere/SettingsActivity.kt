/*
Who Here
Copyright (C) 2020  Josef Hoppe

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published 
by the Free Software Foundation; either version 2 of the License, 
or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package io.hoppe.whohere

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.*
import com.fasterxml.jackson.databind.ObjectMapper
import io.hoppe.whohere.data.ApplicationData
import java.util.*

fun getLanguages(data: ApplicationData): MutableSet<String> {
    val languages = mutableSetOf<String>()
    for (question in data.data) {
        languages.add(question.language)
    }
    return languages
}

fun getDefaultLang(
    languages: Set<String>
): String {
    val defaultLang = Locale.getDefault().language
    return if (languages.contains(defaultLang)) {
        defaultLang
    } else {
        "en"
    }
}

class SettingsActivity() : AppCompatActivity() {

    companion object {
        val HOME_PAGE = "https://gitlab.com/josef.hoppe/who-here/"
        val DATA_PROTECTION = "https://gitlab.com/josef.hoppe/who-here/-/blob/master/datenschutzerklaerung.md"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)

        // Get the Intent that started this activity and extract the string
        val message = intent.getStringExtra(SETTINGS_MESSAGE)
        val mapper = ObjectMapper()
        val data = mapper.readValue<ApplicationData>(message, ApplicationData::class.java)

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.settings, SettingsFragment(data))
            .commit()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    class SettingsFragment(val data: ApplicationData) : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            val context = preferenceManager.context
            val screen = preferenceManager.createPreferenceScreen(context)

            createLangPreferences(context, screen)
            createTypePreferences(context, screen)
            createCategoriesPreferences(context, screen)
            createWhoHerePreferences(context, screen)
            createMiscPreferences(context, screen)

            preferenceScreen = screen
        }

        private fun createLangPreferences(
            context: Context,
            screen: PreferenceScreen
        ): PreferenceCategory {
            val langCategory = PreferenceCategory(context)
            langCategory.key = "language_category"
            langCategory.title = getString(R.string.title_lang)
            screen.addPreference(langCategory)

            val languages = getLanguages(data)
            val langPreference = ListPreference(context)
            val entries = Array(languages.size) { "" }
            val entryValues = Array(languages.size) { "" }
            for ((i, langCode) in languages.withIndex()) {
                entryValues[i] = langCode
                entries[i] = Locale.forLanguageTag(langCode).displayName
            }
            langPreference.entries = entries
            langPreference.entryValues = entryValues
            langPreference.summary = getString(R.string.lang_desc)
            langPreference.key = "lang"
            langPreference.title = getString(R.string.title_lang)
            langPreference.setDefaultValue(getDefaultLang(languages))
            langCategory.addPreference(langPreference)

            return langCategory
        }

        private fun createCategoriesPreferences(
            context: Context,
            screen: PreferenceScreen
        ): PreferenceCategory {
            val categoriesCategory = PreferenceCategory(context)
            categoriesCategory.key = "categories_category"
            categoriesCategory.title = getString(R.string.title_categories)
            screen.addPreference(categoriesCategory)

            val categories = mutableSetOf<String>()
            val languages = mutableSetOf<String>()
            for (question in data.data) {
                languages.add(question.language)
                categories.add(question.category)
            }
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            val lang = prefs.getString("lang", getDefaultLang(languages))
            val translationMap = data.categoryTranslations[lang] ?: mapOf()
            for (c in categories) {
                val categoryPreference = SwitchPreferenceCompat(context)
                categoryPreference.key = "category.$c"
                categoryPreference.title = translationMap[c] ?: c
                categoryPreference.setDefaultValue(ApplicationData.categoryActiveDefault((c)))
                categoriesCategory.addPreference(categoryPreference)
            }
            return categoriesCategory
        }

        private fun createMiscPreferences(
            context: Context,
            screen: PreferenceScreen
        ): PreferenceCategory {
            val miscCategory = PreferenceCategory(context)
            miscCategory.key = "misc_category"
            miscCategory.title = getString(R.string.title_misc)
            screen.addPreference(miscCategory)

            val feedbackPreference = Preference(context)
            feedbackPreference.key = "feedback"
            feedbackPreference.title = getString(R.string.title_feedback_suggestions)
            feedbackPreference.summary = getString(R.string.text_feedback_suggestions)
            feedbackPreference.setOnPreferenceClickListener {
                val intent = Intent(Intent.ACTION_SENDTO)
                intent.data =
                    Uri.parse("mailto:josef@hoppe.io") // only email apps should handle this
                intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback / Suggestion")
                val act = activity
                try {
                    startActivity(intent)
                } catch (e: ActivityNotFoundException) {
                    Toast.makeText(
                        context,
                        "Failed to open Mail Program. Write to josef@hoppe.io",
                        Toast.LENGTH_LONG
                    ).show()
                }
                true
            }
            miscCategory.addPreference(feedbackPreference)

            val licensePreference = Preference(context)
            licensePreference.key = "license"
            licensePreference.title = getString(R.string.title_license)
            licensePreference.summary = getString(R.string.text_license)
            licensePreference.setOnPreferenceClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data =
                    Uri.parse(HOME_PAGE) // should open in browser
                val act = activity

                try {
                    startActivity(intent)
                } catch (e: ActivityNotFoundException) {
                    Toast.makeText(
                        context,
                        "Failed to open Browser. Go to $HOME_PAGE manually.",
                        Toast.LENGTH_LONG
                    ).show()
                }
                true
            }
            miscCategory.addPreference(licensePreference)

            val webVersionPreference = Preference(context)
            webVersionPreference.key = "license"
            webVersionPreference.title = getString(R.string.title_webversion)
            webVersionPreference.summary = getString(R.string.text_webversion)
            webVersionPreference.setOnPreferenceClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data =
                    Uri.parse("https://whohere.hoppe.io") // should open in browser
                val act = activity
                try {
                    startActivity(intent)
                } catch (e: ActivityNotFoundException) {
                    Toast.makeText(
                        context,
                        "Failed to open Browser. Go to whohere.hoppe.io manually.",
                        Toast.LENGTH_LONG
                    ).show()
                }
                true
            }
            miscCategory.addPreference(webVersionPreference)

            val dataProtectionPreference = Preference(context)
            dataProtectionPreference.key = "dataprotection"
            dataProtectionPreference.title = getString(R.string.title_dataprotection)
            dataProtectionPreference.summary = getString(R.string.text_dataprotection)
            dataProtectionPreference.setOnPreferenceClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data =
                    Uri.parse(DATA_PROTECTION) // should open in browser
                val act = activity
                try {
                    startActivity(intent)
                } catch (e: ActivityNotFoundException) {
                    Toast.makeText(
                        context,
                        "Failed to open Browser. Go to $DATA_PROTECTION manually.",
                        Toast.LENGTH_LONG
                    ).show()
                }

            true
            }
            miscCategory.addPreference(dataProtectionPreference)

            val dataVersionPref = Preference(context)
            dataVersionPref.key = "dataversion"
            dataVersionPref.title = getString(R.string.title_version)
            dataVersionPref.summary = "${data.appver.version}, rev.${data.ver}"
            miscCategory.addPreference(dataVersionPref)

            return miscCategory
        }

        private fun createTypePreferences(
            context: Context,
            screen: PreferenceScreen
        ): PreferenceCategory {
            val typesCategory = PreferenceCategory(context)
            typesCategory.key = "types_category"
            typesCategory.title = getString(R.string.title_types_questions)
            screen.addPreference(typesCategory)

            val scalePreference = SwitchPreferenceCompat(context)
            scalePreference.key = "type.scale"
            scalePreference.title = getString(R.string.title_scale)
            scalePreference.setDefaultValue(true)
            typesCategory.addPreference(scalePreference)

            val whoherePreference = SwitchPreferenceCompat(context)
            whoherePreference.key = "type.whohere"
            whoherePreference.title = getString(R.string.title_whohere)
            whoherePreference.setDefaultValue(true)
            typesCategory.addPreference(whoherePreference)

            val textPreference = SwitchPreferenceCompat(context)
            textPreference.key = "type.text"
            textPreference.title = getString(R.string.title_open_questions)
            textPreference.setDefaultValue(true)
            typesCategory.addPreference(textPreference)

            val selectPreference = SwitchPreferenceCompat(context)
            selectPreference.key = "type.select"
            selectPreference.title = getString(R.string.title_select)
            selectPreference.setDefaultValue(true)
            typesCategory.addPreference(selectPreference)
            return typesCategory
        }

        private fun createWhoHerePreferences(
            context: Context,
            screen: PreferenceScreen
        ): PreferenceCategory {
            val whoHereCategory = PreferenceCategory(context)
            whoHereCategory.key = "whohere_category"
            whoHereCategory.title = getString(R.string.title_whohere_advanced)
            screen.addPreference(whoHereCategory)

            val enableAdvancedWhoHerePreference = SwitchPreferenceCompat(context)
            enableAdvancedWhoHerePreference.key = "whohere.enableAdvanced"
            enableAdvancedWhoHerePreference.title = getString(R.string.title_choose_from_list)
            enableAdvancedWhoHerePreference.summary = getString(R.string.summary_advanced_whohere)
            enableAdvancedWhoHerePreference.setDefaultValue(false)
            whoHereCategory.addPreference(enableAdvancedWhoHerePreference)

            val whoherePreference = EditTextPreference(context)
            whoherePreference.key = "whohere.choices"
            whoherePreference.title = getString(R.string.title_whohere_choices)
            whoherePreference.summary = getString(R.string.summary_whohere_choices)
            whoherePreference.setDefaultValue("")
            whoHereCategory.addPreference(whoherePreference)
            return whoHereCategory
        }
    }
}