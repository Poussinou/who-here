/*
Who Here
Copyright (C) 2020  Josef Hoppe

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published 
by the Free Software Foundation; either version 2 of the License, 
or (at your option) any later version.


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package io.hoppe.whohere

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.*
import io.hoppe.whohere.data.ApplicationData
import io.hoppe.whohere.data.Appver
import io.hoppe.whohere.data.Question
import io.hoppe.whohere.network.JsonFetcher
import java.net.URL
import android.widget.RadioButton
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.File
import java.lang.Exception
import androidx.preference.PreferenceManager
import io.hoppe.whohere.data.BundleState
import androidx.core.view.GestureDetectorCompat
import kotlin.math.abs


const val SETTINGS_MESSAGE = "io.hoppe.whohere.SETTINGS_MESSAGE"
const val BUNDLE_STATE = "io.hoppe.whohere.BUNDLE_STATE"

class MainActivity : AppCompatActivity(), GestureDetector.OnGestureListener {
    private var data: ApplicationData = ApplicationData(0, Appver(0, 0, 0), arrayListOf())
    var questions: MutableList<Question> = mutableListOf()
    private val jsonUrl = URL("https://whohere.hoppe.io/api/v1/data")
    private val appVer = Appver(1, 0, 0)
    private val dataFile = "data.json"
    private val mapper = ObjectMapper()
    private var current = 0
    private var hidden = false
    private var sliderMin = 1
    private lateinit var mDetector: GestureDetectorCompat
    val currentQuestion: Question
        get() = if (questions.size > current) {
            questions[current]
        } else {
            Question()
        }

    //Lifecycle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Instantiate the gesture detector with the
        // application context and an implementation of
        // GestureDetector.OnGestureListener
        mDetector = GestureDetectorCompat(this, this)

        val fetcher = JsonFetcher<ApplicationData>({
            // don't load data in an unsupported format
            if (it.appver <= appVer) {
                data = it
                saveData(it)
            } else {
                Toast.makeText(
                    applicationContext,
                    "App out of date. Update to Version ${it.appver.version} or higher",
                    Toast.LENGTH_LONG
                ).show()
                data = loadData()
            }

            if (questions.size == 0) {
                randomizeData()
                updateView()
            }
        }) {
            data = loadData()

            if (questions.size == 0) {
                randomizeData()
                updateView()
            }
        }
        fetcher.execute(jsonUrl as Object, ApplicationData::class.java as Object)

        //set up UI
        val etText = findViewById<EditText>(R.id.etText)
        val tvSliderValue = findViewById<TextView>(R.id.tvSliderValue)
        val sbSlider = findViewById<SeekBar>(R.id.sbSlider)
        val rgSelection = findViewById<RadioGroup>(R.id.rgSelection)

        etText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                currentQuestion.stringAnwser = p0.toString()
            }
        })
        sbSlider.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                currentQuestion.intAnswer = p1
                tvSliderValue.text = (p1 + sliderMin).toString()
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {}
            override fun onStopTrackingTouch(p0: SeekBar?) {}
        })
        rgSelection.setOnCheckedChangeListener { _, i ->
            val rb = findViewById<RadioButton>(i)
            currentQuestion.stringAnwser = rb.text.toString()
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        //recover previous state
        restoreState(savedInstanceState)
    }

    private fun restoreState(savedInstanceState: Bundle) {
        val bundleStateString = savedInstanceState?.getString(BUNDLE_STATE)
        if (bundleStateString != null) {
            val bundleState =
                mapper.readValue<BundleState>(bundleStateString, BundleState::class.java)
            hidden = bundleState.hidden
            current = bundleState.current
            sliderMin = bundleState.sliderMin
            questions = bundleState.questions
            updateView()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onSaveInstanceState(outState: Bundle) {
        val bundleState = BundleState(questions, hidden, current, sliderMin)
        outState.putString(BUNDLE_STATE, mapper.writeValueAsString(bundleState))

        super.onSaveInstanceState(outState)
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        val bundleState = BundleState(questions, hidden, current, sliderMin)
        outPersistentState.putString(BUNDLE_STATE, mapper.writeValueAsString(bundleState))

        super.onSaveInstanceState(outState, outPersistentState)
    }

    // Buttons & Menus

    fun menuSettingsClicked(item: MenuItem) {
        val intentData = mapper.writeValueAsString(data)
        val intent = Intent(this, SettingsActivity::class.java).apply {
            putExtra(SETTINGS_MESSAGE, intentData)
        }
        startActivity(intent)
    }

    fun menuRefreshClicked(item: MenuItem) {
        randomizeData()
        updateView()
    }

    fun menuRulesClicked(item: MenuItem) {
        val intent = Intent(this, RulesActivity::class.java)
        startActivity(intent)
    }

    fun btnNextClicked(view: View) {
        if (questions.size == 0) return
        current = (current + 1) % questions.size
        hidden = false
        updateView()
    }

    fun btnBackClicked(view: View) {
        if (questions.size == 0) return
        current = (current - 1 + questions.size) % questions.size
        hidden = false
        updateView()
    }

    fun btnHideShowClicked(view: View) {
        hidden = !hidden
        updateView()
    }

    //touch interaction

    override fun dispatchTouchEvent(event: MotionEvent?): Boolean {
        return if (super.dispatchTouchEvent(event))
            true
        else
            mDetector.onTouchEvent(event)
    }

    override fun onShowPress(p0: MotionEvent?) = Unit
    override fun onSingleTapUp(p0: MotionEvent?) = false
    override fun onDown(p0: MotionEvent?) = false
    override fun onScroll(p0: MotionEvent?, p1: MotionEvent?, p2: Float, p3: Float) = false
    override fun onLongPress(p0: MotionEvent?) = Unit

    override fun onFling(
        e1: MotionEvent, e2: MotionEvent, rawVelocityX: Float,
        rawVelocityY: Float
    ): Boolean {
        val velocityX = abs(rawVelocityX)
        val xDistance = abs(e1.x - e2.x)
        val yDistance = abs(e1.y - e2.y)
        var result = false

        if (velocityX > 100 && xDistance > 100 && xDistance > 2 * yDistance) {
            if (e1.x > e2.x)
            // right to left
                btnNextClicked(findViewById(R.id.btnNext))
            else
                btnBackClicked(findViewById(R.id.btnBack))

            result = true
        }
        return result
    }

    // helper functions

    private fun loadData(): ApplicationData {
        try {
            val dataFile = File(filesDir, dataFile)
            return mapper.readValue(dataFile, ApplicationData::class.java)
        } catch (e: Exception) {
            return ApplicationData()
        }
    }

    private fun saveData(writeData: ApplicationData) {
        val dataFile = File(filesDir, dataFile)
        mapper.writeValue(dataFile, writeData)
    }

    private fun randomizeData() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        questions = ArrayList(data.data.size)
        val defaultLang = getDefaultLang(getLanguages(data))
        for (q in data.data) {
            if (prefs.getBoolean("category.${q.category}", ApplicationData.categoryActiveDefault(q.category)) &&
                prefs.getBoolean("type.${q.type}", true) &&
                prefs.getString("lang", defaultLang) == q.language
            )
                questions.add(q.copy())
        }
        questions.shuffle()
        current = 0
    }

    private fun updateView() {
        val questionType = currentQuestion.type
        val tvQuestion = findViewById<TextView>(R.id.tvQuestion)
        val etText = findViewById<EditText>(R.id.etText)
        val tvSliderValue = findViewById<TextView>(R.id.tvSliderValue)
        val sbSlider = findViewById<SeekBar>(R.id.sbSlider)
        val rgSelection = findViewById<RadioGroup>(R.id.rgSelection)

        //set question text
        tvQuestion.text = currentQuestion.question

        etText.visibility = View.GONE
        tvSliderValue.visibility = View.GONE
        sbSlider.visibility = View.GONE
        rgSelection.visibility = View.GONE

        when (questionType) {
            "scale" -> {
                sliderMin = currentQuestion.min
                val max = currentQuestion.max

                //init if first display
                val intAnswer = if (currentQuestion.intAnswer == -1) {
                    (max - sliderMin) / 2
                } else {
                    currentQuestion.intAnswer
                }

                //hide or show elements
                if (!hidden) {
                    tvSliderValue.visibility = View.VISIBLE
                    sbSlider.visibility = View.VISIBLE
                } else {
                    tvSliderValue.visibility = View.INVISIBLE
                    sbSlider.visibility = View.INVISIBLE
                }

                //set slider properties
                sbSlider.max = max - sliderMin
                sbSlider.progress = intAnswer
            }
            "text" -> {
                if (!hidden) {
                    etText.visibility = View.VISIBLE
                } else {
                    etText.visibility = View.INVISIBLE
                }

                etText.setText(currentQuestion.stringAnwser)
            }
            "whohere" -> {
                val prefs = PreferenceManager.getDefaultSharedPreferences(this)
                if (prefs.getBoolean("whohere.enableAdvanced", false)) {
                    val defaultChoices = getString(R.string.msg_select_choices)
                    configRadioGroup(
                        rgSelection,
                        (prefs.getString("whohere.choices", defaultChoices) ?: defaultChoices)
                            .split("\n"),
                        currentQuestion.stringAnwser,
                        hidden
                    )
                } else {
                    if (!hidden) {
                        etText.visibility = View.VISIBLE
                    } else {
                        etText.visibility = View.INVISIBLE
                    }
                    etText.setText(currentQuestion.stringAnwser)
                }

            }
            "select" -> {
                configRadioGroup(
                    rgSelection,
                    currentQuestion.choices,
                    currentQuestion.stringAnwser,
                    hidden
                )
            }
        }
    }

    private fun configRadioGroup(
        rgSelection: RadioGroup,
        choices: List<String>,
        selectedChoice: String,
        hidden: Boolean
    ) {
        rgSelection.removeAllViews()

        for (choice in choices) {
            val rb = RadioButton(this)
            rb.text = choice
            rb.textSize = 30.0f
            rb.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            rgSelection.addView(rb)
            //hidden: don't offer a choice and hide the choice
            if (!hidden && choice == selectedChoice) {
                rb.isChecked = true
            }
            if (hidden) {
                rb.isEnabled = false
            }
        }

        rgSelection.visibility = View.VISIBLE
    }
}
