/*
Who Here
Copyright (C) 2020  Josef Hoppe

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published 
by the Free Software Foundation; either version 2 of the License, 
or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package io.hoppe.whohere.data

import androidx.annotation.Keep
import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties("questionThirdPerson")
@Keep
data class Question (
	/**
	 * Question type. depending on the type, other attributes have to be set
	 *
	 * - "select": choices should be a non-empty list of the choices
	 * - "text" (nothing)
	 * - "whohere" (nothing
	 * - "scale": min and max should be set
	 */
	val type : String = "none",
	/**
	 * Language, used for filtering in Settings
	 */
	val language: String = "na",
	/**
	 * minimum for type scale
	 */
	val min : Int = 0,
	/**
	 * maximum for type scale
	 */
	val max : Int = 0,
	/**
	 * choices for type select
	 */
	val choices : List<String> = listOf(),
	/**
	 * Question text
	 */
	val question : String = "",
	/**
	 * Question category, any String allowed
	 */
	val category : String = "none",
	/**
	 * the answer given in the app (if it is a String), not sent by server
	 */
	var stringAnwser : String = "",
	/**
	 * the answer given in the app (if it is an Int), not sent by server
	 */
	var intAnswer : Int = -1
)