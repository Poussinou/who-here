/*
Who Here
Copyright (C) 2020  Josef Hoppe

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published 
by the Free Software Foundation; either version 2 of the License, 
or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package io.hoppe.whohere.data

import androidx.annotation.Keep
import com.fasterxml.jackson.annotation.JsonIgnore

@Keep
data class Appver (
	@Keep val major : Int = 0,
	@Keep val minor : Int = 0,
	@Keep val build : Int = 0
) {
	operator fun compareTo(other: Appver): Int {
		if(major == other.major && minor == other.minor && build == other.build) return 0
		if(major > other.major ||
			(major == other.major &&
					(minor > other.minor ||
							(minor == other.minor && (build > other.build))))) return 1
		return -1
	}

	/**
	 * major.minor.build
	 */
	@get:JsonIgnore
	val version: String
		get() = "$major.$minor.$build"
}