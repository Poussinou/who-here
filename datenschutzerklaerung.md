# Datenschutzerklärung 

(see below for english data protection statement)

Der Fragenkatalog wird bei jedem Start über das Internet aktualisiert. Die
IP-Adresse wird dabei vom Server nicht gespeichert.

Zu keinem Zeitpunkt werden Daten von der App versendet. Die App speichert 
Einstellungen dauerhaft lokal auf dem Gerät. Antworten werden nur bis zum
nächsten Beenden der App lokal gespeichert. Es werden auch keinerlei Statistiken
erhoben oder versandt.

# Data protection statement

The app updates the question database from a server on the internet everytime
it starts. The IP address of the request is not logged by the server.

No data is sent by the app. Settings are saved to a local storage indefinitely.
Answers given are only kept until the next restart of the app. No answers or
statistics are sent to the creator.